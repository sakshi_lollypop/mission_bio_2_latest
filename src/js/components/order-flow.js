//Custom screen flows
$(".flow-next").click(function () {
  var instance = $('#first').parsley();
  var yes = instance.isValid();
  var flow = $(this).parents(".order__flow");
  var flow2 =  $(".order").children(".order__flow").eq(3);
  var list = $(".cutom-flows").find("li")
  var text = $("#first").val();
  var progress = $(".progress-circle").children();


  //Parsley validation
  $('input').each( function() {
      if ($(this).parsley().validate() !== true);
  });
  if (yes) {
    
    flow.removeClass("js-show").addClass("js-hide");
    flow.next().removeClass("js-hide").addClass("js-show");
    $(".target-status--head").html(text);
    if($(this).parents(".order__flow").is(":nth-child(1)")) {
      $(".target-status").children().removeClass("js-hide");
      $(".target-status span").addClass("red350").html("DRAFT");
      progress.eq(0).find("span").addClass("js-bgw");
      $(".progress-value--per").removeClass("js-hide");
      $(".progress-value--blank").addClass("js-hide");
    }
    else if($(this).parents(".order__flow").is(":nth-child(2)")) {
      $(this).parents(".section").addClass("js-hide");
      $(".step3").removeClass("js-hide"); 
      $(".edit").addClass("js-hide");
      progress.eq(1).find("span").addClass("js-bgw");
    }
    else if($(this).hasClass("submit")) {
      $(this).parents(".section").addClass("js-hide");
      flow2.parents(".section").removeClass("js-hide");
      $(".target-status span").removeClass("red350").addClass("green").html("SUBMITTED");
      list.next("li:first").addClass("active").find(".flow-circle").addClass("bg_sky");
      $(".progress-value--per").text("45%");
      progress.eq(2).find("span").addClass("js-bgw");
    }
    else if($(this).parents(".order__flow").is(":nth-child(3)")) {
      $(".target-status span").addClass("orange_text").removeClass("green").html("RESULT READY");
      $(".progress-value--per").text("95%");
      progress.eq(3).find("span").addClass("js-bgw");
      progress.eq(4).find("span").addClass("js-bgw");
      progress.eq(5).find("span").addClass("js-bgw");
      progress.eq(6).find("span").addClass("js-bgw");
      progress.eq(7).find("span").addClass("js-bgw");
      
     
    }
    else if($(this).parents(".order__flow").is(":last-child")) {
      $(this).parents(".order__flow").removeClass("js-hide").addClass("js-show");
      list.next("li:last").addClass("active").find(".flow-circle").addClass("bg_sky");
    }
  }
  else {
    return false;
  }
});

//Last Order submission
$("#order-form").submit(function(e) {
  e.preventDefault();
  var progress = $(".progress-circle").children();
  progress.eq(8).find("span").addClass("js-bgw");
  $(".progress-value--per").text("100%");
  $(".target-status span").addClass("flblue").removeClass("orange_text").html("ORDERED");
  $(this).parents(".order__flow").find(".result-feedback").removeClass("js-hide");
  $(this).parents(".order__flow").find(".request-quote").addClass("js-hide");
});


// Apend target summary table to overview screen
$(".custom-design-flow__items").click(function () {
  $(".custom-design-flow__items").removeClass("active-item");
  $(this).addClass("active-item");
  $(this).parents(".section").find(".matches").addClass("js-hide");
  if($(this).is(':first-child')) {
    $(this).parents(".section").find(".overview").removeClass("js-hide");
    if($(this).parents(".section").find(".target-table-sm").length === 1) {
      return false;
    }
    else if($(this).parents(".section").find(".target-table-sm").length === 0) {
      $(".target-table-sm").clone().appendTo(".overview-table");
      $(".succed-target").addClass("js-hide");
      $(this).parents(".section").find(".target-table-sm").find(".add-target").remove();
      $(this).parents(".section").find(".target-table-sm").find(".target-summary-tag").removeClass("js-hide");
    }
  }
  else if($(this).is(':last-child')){
    $(this).addClass("last")
    $(this).parents(".section").find(".matches").removeClass("js-hide");
    $(this).parents(".section").find(".overview").addClass("js-hide");
  }
});
//Edit target flow
$(".edit-targets").click(function() {
  var s = $(this).parents(".step3").prev(".section");
  $(this).parents(".section").removeClass("js-show").addClass("js-hide");
  s.removeClass("js-hide");
  s.find(".order__flow:nth-child(2)").removeClass("js-hide").addClass("js-show");
  s.find(".order__flow:nth-child(3)").addClass("js-hide").removeClass("js-show");
});


