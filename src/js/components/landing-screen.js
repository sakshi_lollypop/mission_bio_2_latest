//Zoom effects on panel list
$(document).ready(function() {
    $(".panel-card__items").on("mouseenter", function() {
        $(this).addClass("zoom-out");
        if($(this).is(':first-child')) {
        $(this).parents(".target-panels").find(".circle").addClass("transform-or-rh")
        }
        else if($(this).is(':last-child')) {
            $(this).parents(".target-panels").find(".circle").addClass("transform-or-lh")
        }
    }); 
    $(".panel-card__items").on("mouseleave", function() {
        $(this).removeClass("zoom-out");
        $(".circle").removeClass("transform-or-rh");
        $(".circle").removeClass("transform-or-lh");
    });
});
// Show pannels 
    $(".panel-card__items a").click(function () {
        var g = $(this).attr("href");
        $(this).parents(".section").removeClass("pad-tp10")
        $(this).parents(".section").next().removeClass("js-hide");
        $(this).parents(".section").next().find(g).addClass("js-show");
        $(this).parents(".target-panels").addClass("js-hide");
    });
    $(".panel-close").click(function () {  
        $(this).parents(".section").prev().removeClass("pad-tp10") 
      $(this).parents(".fixed-panel-tabel").removeClass("js-show");
      $(this).parents(".custom-panel-tabel").removeClass("js-show");
      $(".target-reuser").removeClass("js-hide");
      $('html, body').animate({ scrollTop: 0 }, 'slow',function() {
      });
  });

// Show pannels for returnig user
    $(".target-reuser--link").click(function () {
        var g = $(this).attr("href");
        $(this).parents(".section").removeClass("pad-tp10")
        $(this).parents(".section").next().removeClass("js-hide");
        $(this).parents(".section").next().find(g).addClass("js-show");
        $(this).parents(".target-reuser").addClass("js-hide");
    });
    $(".panel-close").click(function () {   
      $(this).parents(".section").prev().addClass("pad-tp10")
      $(this).parents(".fixed-panel-tabel").removeClass("js-show");
      $(this).parents(".custom-panel-tabel").removeClass("js-show");
      $(this).parents(".section").addClass("js-hide");
      $(".target-panels").removeClass("js-hide");
      $('html, body').animate({ scrollTop: 0 }, 'slow',function() {
      });
  });

// TABS for custom-panel right
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
        $(this).addClass("border-blue-btm")
        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

// $(document).ready(function(){
//     $('.custom-design-flow.tabs li').click(function(){
//         var tab_id = $(this).attr('data-tab');
//         $(this).addClass("border-blue-btm")
//         $('.custom-design-flow.tabs li').removeClass('current');
//         $('.tab-content').removeClass('current');

//         $(this).addClass('current');
//         $("#" + tab_id).addClass('current');
//     })
// });
    
//Tooltiip
$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        animation: true,
        delay: {show: 10, hide: 100}  
    })
})

 // hide any open tooltip when the anywhere else in the body is clicked
$('body').on('click', function (e) {
    $('[data-toggle="tooltip"]').each(function () {  
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.tooltip').has(e.target).length === 0) {
            $(this).tooltip('hide');
        }
    });
});
  
// Showing panel names
$(window).load(function () {
    var name =$(".panel-nav").find(".breadcrumb-item.active");
    if(name) {
        var name2 = name.text();
        $(".target-status--head").text(name2);
      
    }
             
});
$(window).load(function () {
    var head =$(".side-bar").find(".target-status--head").text();
    if(head == "Catalog Panel") {
        $(".side-bar").find(".panel-lock").addClass("js-show");
    }
          
});
//Show matched screens
$(".next-match").click(function () {
    var screens = $(".matched-list").find(".matched-list__items:first");
    screens.removeClass("js-hide").addClass("js-show");
});

//Sticky initialization
$(window).load(function () {
    $('.ui.sticky').sticky({
        offset: 132,
        context: '#sticky-content'
    })
});

//Sticky initialization
$(window).on('load resize', function () {
    if (window.matchMedia("(min-width: 992px)").matches) {
        $('.flow-next').click(function () {
            $('.ui.sticky').sticky({
                offset: 132,
                context: '#sticky-content'
            })
        });

    }
});


    

// Submit Button Disabled and Enabled - Create Password
$(document).ready(function () {
    $('.add-text').click(function () {
    if ($('#submitbtn').is(':disabled')) {
        $('#submitbtn').removeAttr('disabled');
        $("#submitbtn").removeClass("opacity_5");

    } else {
        $('#submitbtn').attr('disabled', 'disabled');
        $("#submitbtn").addClass("opacity_5");

    }
});



//     if ($(".add-text").text().length >= 1) {
//     $("#submitbtn").prop("disabled", false);
//     $("#submitbtn").removeClass("opacity_5");
//     }
// else {
//     $("#submitbtn").prop("disabled", true);
//     $("#submitbtn").addClass("opacity_5");
// }
});