// Onboarding Slider

// window-resize-slider-strech
$(window).resize(function () {
    $('.onboardingJS')[0].slick.refresh();
    console.log("aaaa");
});

$(document).ready(function () {

    $('.modal').on('shown.bs.modal', function (e) {
        $('.onboardingJS').resize();
        console.log("ccc");
    })

    $('.onboardingJS').not('.slick-initialized').slick({
        lazyLoad: 'ondemand',
        autoplay: false,
        infinite: false,
        arrows: true,
        dots: true,
        slidesToShow: 1,
        fade: true
    });

});
