// Table Row- Delete,Edit,Save functions
$(document).ready(function () {
  // Delete row functionality
  $('.js-delete-row').click(function () {
    var eror = $("table").find(".error-circle");
    var num = eror.length;
    var rows = $(".table--edit").find("tr");
    var succedTr = $(".succed-target").children().eq(0);
    var f = $(".succed-target").children().eq(2);
    var rowNums = rows.length;
    var num = eror.length;
    var succedRows = rowNums - num;
    // succedTr.html(succedRows);



    $(this).closest('tr').remove();
    rowNums--;
    succedRows = rowNums - num;
    // console.log(rowNums)
    // // succedRows++;
    f.html(rowNums);
    succedTr.html(succedRows);
    if($(this).parents("tr").hasClass("error-circle")) {
      $(this).parents(".error-circle").removeClass();
      num--;
      succedRows = rowNums - num;
      succedTr.html(succedRows);
    $(".error-target__item:first").html(num);
    if($(num).length === 0) {
        $(".flow-disabled").prop("disabled", false);
        $(".flow-disabled").removeClass("opacity_5");
        $(".error-target").addClass("js-hide");
      }
      else {
        $(".flow-disabled").prop("disabled", true);
        $(".flow-disabled").addClass("opacity_5");
      }
    }
    else {
      return false;
    }
  });
});

  // Add new row in a table on click function
//   $("#addTarget").click(function () {

    
//     $("#addTargetTable").each(function () {

//       var tds = '<tr>';
//       jQuery.each($('tr:last td', this), function () {
//         tds += '<td>' + $(this).html() + '</td>';
//       });
//       tds += '</tr>';
//       if ($('tbody', this).length > 0) {
//         $('tbody', this).prepend(tds);
//       } else {
//         $(this).prepend(tds);
//       }
//     });
//   });
// });


$("#addTarget").click(function (){

  var newRow = `<tr>
                <td class="text-uppercase">fgh</td>
                <td class="text-uppercase">NM_000155(GAA):YUE7C</td>
                <td class="text-capitalize"> <a class="link fnt-sm" href="javascript:void(0)">link</a>
                  <div class="edit">
                    <div class="custom-icon icon-pencil mr-4 editbtn">
                    </div>
                    <img class="saveBtn js-hide" src="img/icons/Save.svg" alt="Save">
                    <div class="custom-icon icon-trash js-delete-row"></div>
                  </div>
                </td>
              </tr>`

  $('.table--edit').append(newRow);
});

// Edit table row data
$('.editbtn').click(function () {
  var currentTD = $(this).parents('tr').find('td');
  $(this).parents(".edit").find(".icon-trash").removeClass("custom-icon").addClass("js-hide");
  $(this).parents(".edit").find(".editbtn").removeClass("custom-icon").addClass("js-hide");
  $(this).parents(".edit").find(".saveBtn").removeClass("js-hide");
  $(this).parents(".edit").removeClass("js-hide");
  currentTD = $(this).parents('tr').find('td:lt(2)');
  $.each(currentTD, function () {
    $(this).prop('contenteditable', true)
  });
  });

//Save table row data
$(".saveBtn").click(function() {
  var eror = $("table").find(".error-circle");
  var rows = $(".table--edit").find("tr");
  var succedTr = $(".succed-target").children().eq(0);
  var rowNums = rows.length;
  var num = eror.length;
  var succedRows = rowNums - num;
  succedTr.html(succedRows);
  $(this).parents(".edit").find(".icon-trash").removeClass("js-hide").addClass("custom-icon ");
  $(this).parents(".edit").find(".editbtn").removeClass("js-hide").addClass("custom-icon");
  $(this).addClass("js-hide");
  var currentTD = $(this).parents('tr').find('td');
    currentTD = $(this).parents('tr').find('td');
    $.each(currentTD, function () {
      $(this).prop('contenteditable', false)
    });
    if($(this).parents("tr").hasClass("error-circle")) {
      $(this).parents(".error-circle").removeClass();
      num--;
      succedRows++;
      succedTr.html(succedRows);
    $(".error-target__item:first").html(num);
    if($(num).length === 0) {
        $(".flow-disabled").prop("disabled", false);
        $(".flow-disabled").removeClass("opacity_5");
        $(".error-target").addClass("js-hide");
      }
      else {
        $(".flow-disabled").prop("disabled", true);
        $(".flow-disabled").addClass("opacity_5");
      }
    }
    else {
      return false;
    }
    
});
//Succed target 
$(document).ready(function () {
  var row = $(".table--edit").find("tr");
  var rowNum = row.length;
  var eror = $("table").find(".error-circle");
  var num = eror.length;
  var succedRow;
  var f = $(".succed-target").children().eq(2);
  var h = $(".succed-target").children().eq(0);
  f.html(rowNum);
  succedRow = rowNum - num;
  h.html(succedRow);

});

  


