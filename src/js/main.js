// dropdown and checkbox intialization
import './components/landing-screen';

import './components/order-flow';

// Onboarding Slider
import './components/slick-init';

// Table Row- Delete,Edit,Save functions
import './components/table';

// Bodymoving JS
import './components/bodymovin-init';
(function ($, window, document, undefined) {
    "use strict";
    // console.log("main.js is loaded");

    /* Initialize FORMS components */

    if ($('.ui.dropdown').length) {
        $('.ui.dropdown').dropdown({ forceSelection: false });
    }

    if ($('.ui.checkbox').length) {
        $('.ui.checkbox').checkbox();
    }
    if ($('.js-tooltip').length) {
        $('.js-tooltip').tooltip({});
    }

    $(window).on('beforeunload', function () {
        $(window).scrollTop(0);
    });
})(jQuery, window, document);


